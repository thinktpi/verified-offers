# James Changelog


## 1.0.3 - 03 Aug 2016
* Fix accessibility HTML markup for widget titles      
* Fix link target _self for CTA widgets icons   
* Fix CSS vertical alignment for testimonial widget on the Home page with different length testimonials                    

### Files Modified
* /style.css  
* /style-rtl.css  
* /languages/*
* /lib/init.php  
* /lib/widgets/call-to-action.php  
* /lib/widgets/widget-social.php  
* /lib/widgets/wsm-featured-testimonial.php  
* /lib/widgets/wsm-featured-widget.php  
* /lib/widgets/wsm-sidebar-button.php      

---

## 1.0.2 - 02 May 2016
* Fix bug in Google Fonts enqueue script    
* CSS fix to accommodate correct fonts in header  
* Update screenshot to match theme (again. Grrrr.)                  

### Files Modified
* /style.css  
* /style-rtl.css  
* /lib/init.php  
* /screenshot.png    

---

## 1.0.1 - 28 Apr 2016
* Bug fix in site title and description layout  
* CSS fix in home page testimonials layout for Internet Explorer
* Update screenshot to match theme                  

### Files Modified
* /style.css  
* /style-rtl.css  
* /screenshot.png    

---

## 1.0.0 - 27 Apr 2016
* Initial theme release