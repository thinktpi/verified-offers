<?php

/**

Template Name: Testimonial

**/

//* Force full-width-content layout setting
add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_full_width_content' );

// Execute custom child loop
add_action( 'genesis_loop', 'james_testimonial_loop' );
function james_testimonial_loop() {

	global $wp_query;
	global $post;

	$page_banner_image = get_post_meta( $post->ID, '_james_page_banner_image', true );
	$page_banner_default_image = genesis_get_option( 'wsm_page_top_image', 'james-settings' );
	$page_banner_hide_image = get_post_meta( $post->ID, '_james_page_hide_image', true );

	$args = array(
			'post_type' => 'james-testimonial',
			'posts_per_page' => '50',
			'paged'          => get_query_var( 'paged' ),
			'order' => 'DESC',
		);

	$wp_query = new WP_Query( $args );

	if( $wp_query->have_posts() ):

	if ( !empty( $page_banner_hide_image ) ) {
		the_title( '<h1 class="entry-title">', '</h1>' );
	}


	while ( $wp_query -> have_posts() ) : $wp_query -> the_post();


		global $post;

		$testimonial_img = genesis_get_image( array(
			'format' => 'html',
			'size' => 'testimonials',
			'attr' => array( 'class' => 'author-image alignleft' ),
		 ) );

		$company = get_post_meta( $post->ID, '_customer_name', true );

		$toggle = $toggle == 'odd' ? 'even' : 'odd';

		$post_id = get_the_ID( $post->ID );

			echo '<div class="entry-testimonial ' . $toggle . ' testimonial-' . $post_id . '">';
					if( !empty( $testimonial_img ) ) {
						echo '<div class="testimonial-photo">' . $testimonial_img . '</div>';
					}
					echo '<blockquote>' . $post->post_content . '</blockquote>';
					echo '<div class="credit">';
					echo '<span class="author">' . get_the_title( $post->ID ) . '</span>';
					if( !empty( $company ) ) {
						echo '<span class="company-name">' . $company . '</span>';
					}
					echo '</div>';
			echo '</div>';

	endwhile;

	genesis_posts_nav();

	endif;

	//* Restore original query
	wp_reset_query();

}

remove_action( 'genesis_loop', 'genesis_do_loop' );


genesis();