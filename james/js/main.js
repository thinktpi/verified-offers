jQuery("document").ready(function($){
	
	do_site_header();
	
	$(window).scroll(function () {
		do_site_header();
	});

	function do_site_header(){
		var nav = $('.site-header');

		if ($(document).scrollTop() > 136) {
			nav.addClass("f-nav");
		} else {
			nav.removeClass("f-nav");
		}
	}

});