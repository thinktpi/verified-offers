<?php

/**

Template Name: Contact

**/

remove_action( 'genesis_before_footer', 'james_interior_footer_cta', 9 );
remove_action( 'genesis_after_header' , 'james_do_top_banner' , 10 );

// Do Map Section
add_action( 'genesis_after_header' , 'james_do_map_code' );
function james_do_map_code() {

	global $post;

	$map_code =  get_post_meta( $post->ID, '_wsm_contact_map', true );

	if ( !empty( $map_code ) ) {
		echo '<div class="contact-map">'. do_shortcode( $map_code ) .'</div>';
	}

}

//* remove the banner image if submitting a contact form
if ( ! empty( $_POST ) ) {

	remove_action( 'genesis_after_header' , 'james_do_map_code' );

}


genesis();