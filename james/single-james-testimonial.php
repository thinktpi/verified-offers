<?php

//* Remove Post Info
remove_action( 'genesis_entry_header', 'genesis_post_info', 8 );

//* Remove the author box on single posts HTML5 Themes
remove_action( 'genesis_after_entry', 'genesis_do_author_box_single', 8 );

//* Remove the post content (requires HTML5 theme support)
remove_action( 'genesis_entry_content', 'genesis_do_post_content' );

add_action ( 'genesis_entry_content', 'testimonial_author_image' );
function testimonial_author_image() {

	global $post;

	$testimonial_img = genesis_get_image( array(
		'format' => 'html',
		'size' => 'testimonials',
		'attr' => array( 'class' => 'author-image alignleft' )
	) );

	if( !empty( $testimonial_img ) ) {
		echo '<div class="testimonial-photo">'. $testimonial_img .'</div>';
	}

	echo '<blockquote>' . $post->post_content . '</blockquote>';

}

genesis();