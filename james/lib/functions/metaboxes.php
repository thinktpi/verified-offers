<?php
/**
 * Include and setup custom metaboxes and fields. (make sure you copy this file to outside the CMB directory)
 *
 * Be sure to replace all instances of 'yourprefix_' with your project's prefix.
 * http://nacin.com/2010/05/11/in-wordpress-prefix-everything/
 *
 * @category YourThemeOrPlugin
 * @package  Metaboxes
 * @license  http://www.opensource.org/licenses/gpl-license.php GPL v2.0 (or later)
 * @link     https://github.com/WebDevStudios/CMB2
 */

/**
 * Get the bootstrap! If using the plugin from wordpress.org, REMOVE THIS!
 */


$post_id = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'] ;
$template_file = get_post_meta($post_id,'_wp_page_template',TRUE);

if ( file_exists( CHILD_DIR . '/lib/metabox/init.php' ) ) {
	require_once CHILD_DIR . '/lib/metabox/init.php';
} elseif ( file_exists( dirname( __FILE__ ) . '/cmb2/init.php' ) ) {
	require_once dirname( __FILE__ ) . '/cmb2/init.php';
} elseif ( file_exists( dirname( __FILE__ ) . '/CMB2/init.php' ) ) {
	require_once dirname( __FILE__ ) . '/CMB2/init.php';
}



// Register Home Page Metabox
if ( $template_file == 'page_home.php') {
	add_action( 'cmb2_init', 'james_register_home_metabox' );
}

function james_register_home_metabox() {

	// Start with an underscore to hide fields from custom fields list
	$prefix = '_james_home_';

	$cmb_home_metabox = new_cmb2_box( array(
		'id'           => $prefix . 'metabox',
		'title'        => __( 'Home Top Image', 'james' ),
		'object_types' => array( 'page', ), // Post type
		'context'      => 'normal',
		'priority'     => 'high',
		//'show_names'   => true, // Show field names on the left
	) );

	$cmb_home_metabox->add_field( array(
		'name' => __( 'Banner Image URL', 'james' ),
		'id'   => $prefix . 'banner_image',
		'type' => 'file',
	) );

	$cmb_home_metabox->add_field( array(
		'name' => __( 'Banner Image Alt Text', 'james' ),
		'id'   => $prefix . 'banner_image_alt',
		'type' => 'text_medium',
	) );

	$cmb_home_metabox->add_field( array(
		'name' => __( 'Banner Title', 'james' ),
		'id'   => $prefix . 'banner_title',
		'type' => 'textarea_small',
	) );

	$cmb_home_metabox->add_field( array(
		'name' => __( 'Banner Text', 'james' ),
		'id'   => $prefix . 'banner_text',
		'type' => 'textarea_small',
	) );

}

// Register Services Page Metabox
if ( $template_file == 'page_contact.php') {
	add_action( 'cmb2_init', 'wsm_register_contact_metabox' );
}

/**
 * Interior Hero Image Metabox
 */
function wsm_register_contact_metabox() {

	// Start with an underscore to hide fields from custom fields list
	$prefix = '_wsm_contact_';

	$cmb_contact = new_cmb2_box( array(
		'id'           => $prefix . 'metabox',
		'title'        => __( 'Contact Page Details', 'james' ),
		'object_types' => array( 'page',), // Post type
		'context'      => 'normal',
		'priority'     => 'high',
		'show_names'   => true, // Show field names on the left
	) );

	$cmb_contact->add_field( array(
		'name' => __( 'Map Embed Code', 'james' ),
		'id'   => $prefix . 'map',
		'type' => 'textarea_code',
	) );

}

// Register Page Metabox

if ( $template_file != 'page_home.php' && $template_file != 'page_contact.php') {
	add_action( 'cmb2_init', 'james_register_page_metabox' );
}

function james_register_page_metabox() {

	$prefix = '_james_page_';

	$cmb_page_metabox = new_cmb2_box( array(
		'id'           => $prefix . 'metabox',
		'title'        => __( 'Page Top Image', 'james' ),
		'object_types' => array( 'page',), // Post type
		'context'      => 'normal',
		'priority'     => 'high',
	) );

	$cmb_page_metabox->add_field( array(
		'name' => __( 'Banner Image URL', 'james' ),
		'id'   => $prefix . 'banner_image',
		'type' => 'file',
	) );

	$cmb_page_metabox->add_field( array(
		'name' => __( 'Hide Top Image', 'james' ),
		'id'   => $prefix . 'hide_image',
		'type' => 'checkbox',
	) );

}

