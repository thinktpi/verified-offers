<?php
/**
 * Child after header
 *
 * @category     Child
 * @package      Structure
 * @author       Web Savvy Marketing
 * @license      http://opensource.org/licenses/gpl-2.0.php GNU Public License
 * @since        2.0.0
 */


// Add Page Top Banner image

add_action( 'genesis_after_header' , 'james_do_top_banner' , 10 );
function james_do_top_banner() {
	global $post;

	$page_banner_image = get_post_meta($post->ID, '_james_page_banner_image', true );
	$page_banner_default_image = genesis_get_option( 'wsm_page_top_image', 'james-settings' );
	$page_banner_hide_image = get_post_meta( $post->ID, '_james_page_hide_image', true );


	// skip all the other checks if the image should be hidden
	if ( ! empty( $page_banner_hide_image ) )
		return;

	if ( !is_front_page() && is_page() ) {

		if ( !empty( $page_banner_image ) ) {

			//* Remove the entry title (requires HTML5 theme support)
			remove_action( 'genesis_entry_header', 'genesis_do_post_title' );

			echo '<div class="top-image page-image">';
			echo '<div class="wrap">';
			echo '<img src="' . $page_banner_image . '" alt="' . get_the_title( $post->ID ) . '" class="banner-image" />';
			the_title( '<h1 class="entry-title">', '</h1>' );
			echo '</div></div>';
		}

		elseif ( !empty( $page_banner_default_image ) ) {

			//* Remove the entry title (requires HTML5 theme support)
			remove_action( 'genesis_entry_header', 'genesis_do_post_title' );

			echo '<div class="top-image page-image default-image" >';
			echo '<div class="wrap">';
			echo '<img src="' . do_shortcode( $page_banner_default_image ) . '" alt="' . get_the_title( $post->ID ) . '" class="banner-image page-banner" />';
			the_title( '<h1 class="entry-title">', '</h1>' );
			echo '</div></div>';
		}

	}


}