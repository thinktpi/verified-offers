<?php
/**
 * Child Comment Form
 *
 * This file calls the defines the output for the HTML5 blog comment form.
 *
 * @category     Child
 * @package      Structure
 * @author       Web Savvy Marketing
 * @license      http://opensource.org/licenses/gpl-2.0.php GNU Public License
 * @since        2.0.0
 */

/** Edit comments form text **/

add_filter( 'comment_form_defaults', 'james_genesis_comment_form_args', 5 );

function james_genesis_comment_form_args( $defaults ) {

	global $user_identity, $id;

	$commenter = wp_get_current_commenter();
	$req       = get_option( 'require_name_email' );
	$aria_req  = ( $req ? ' aria-required="true"' : '' );

	$author = '<p class="comment-form-author">' .
			 '<label for="author">' . __( 'Name', 'james' ) .   ( $req ? '<span class="required">*</span>' : '' ) .'</label> ' .
			 '<input id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30" tabindex="1"' . $aria_req . ' />' .
			 '</p><!-- .form-section-author .form-section -->';

	$email = '<p class="comment-form-email">' .
			'<label for="email">' . __( 'Email', 'james' ) . ( $req ? '<span class="required">*</span>' : '' ) . '</label> ' .
			'<input id="email" name="email" type="text" value="' . esc_attr( $commenter['comment_author_email'] ) . '" size="30" tabindex="2"' . $aria_req . ' />' .
			'</p><!-- .form-section-email .form-section -->';

		$url = '<p class="comment-form-url">' .
		 '<label for="url">' . __( 'Website', 'james' ) . '</label> ' .
			'<input id="url" name="url" type="text" value="' . esc_attr( $commenter['comment_author_url'] ) . '" size="30" tabindex="2" />' .
	         '</p><!-- .form-section-url .form-section -->';

	$comment_field = '<p class="comment-form-comment">' .
					'<label for="comment">' . __( 'Comment', 'james' ) . '</label>' .
	                  '<textarea id="comment" name="comment"  cols="45" rows="8" tabindex="4" aria-required="true"></textarea>' .
	                 '</p>';

	$args = array(
		'fields'               => array(
			'author' => $author,
			'email'  => $email,
			'url'    => $url,
		),
		'comment_field'        => $comment_field,
		'title_reply'          => __( 'Leave a Comment', 'james' ),
		'label_submit' => __( 'Submit', 'james' ), //default='Post Comment'
		'title_reply_to' => __( 'Reply', 'james' ), //Default: __( 'Leave a Reply to %s' )
		'cancel_reply_link' => __( 'Cancel', 'james' ),//Default: __( 'Cancel reply' )
		'comment_notes_before' => '',
		'comment_notes_after'  => '',
	);

	/** Merge $args with $defaults */
	$args = wp_parse_args( $args, $defaults );

		/** Return filterable array of $args, along with other optional variables */
	return apply_filters( 'genesis_comment_form_args', $args, $user_identity, $id, $commenter, $req, $aria_req );

}

// Remove comments from Post types
add_action( 'init', 'james_remove_store_comments', 10 );
function james_remove_store_comments() {
    remove_post_type_support( 'store_page', 'comments' );
}


// Edit Comment Lists
remove_action( 'genesis_list_comments', 'genesis_default_list_comments' );
add_action( 'genesis_list_comments', 'james_list_comments' );
function james_list_comments() {

	$defaults = array(
		'type'        => 'comment',
		'avatar_size' => 110,
		'format'      => 'html5', //* Not necessary, but a good example
		'callback'    => genesis_html5() ? 'james_html5_comment_callback' : 'genesis_comment_callback',
	);

	$args = apply_filters( 'genesis_comment_list_args', $defaults );

	wp_list_comments( $args );

}


function james_html5_comment_callback( $comment, array $args, $depth ) {

	$GLOBALS['comment'] = $comment; ?>

	<li <?php comment_class(); ?> id="comment-<?php comment_ID(); ?>">
	<article <?php echo genesis_attr( 'comment' ); ?>>

		<?php do_action( 'genesis_before_comment' ); ?>

		<header <?php echo genesis_attr( 'comment-header' ); ?>>


			<p <?php echo genesis_attr( 'comment-author' ); ?>>
				<?php
				echo get_avatar( $comment, $args['avatar_size'] );

				$author = get_comment_author();
				$url    = get_comment_author_url();

				echo' <span '.  genesis_attr( 'comment-meta' ).'>';

				$d = 'm.d.y';

				printf( '<time %s>', genesis_attr( 'comment-time' ) );
				printf( '<a href="%s" %s>', esc_url( get_comment_link( $comment->comment_ID ) ), genesis_attr( 'comment-time-link' ) );
				echo    esc_html( get_comment_date($d) ) . ' ' . __( 'at', 'genesis' ) . ' ' . esc_html( get_comment_time() );
				echo    '</a></time>';
				edit_comment_link( __( '(Edit)', 'genesis' ), ' ' );

				echo '</span>';


				if ( ! empty( $url ) && 'http://' !== $url ) {
					$author = sprintf( '<a href="%s" %s>%s</a>', esc_url( $url ), genesis_attr( 'comment-author-link' ), $author );
				}

				/**
				 * Filter the "comment author says" text.
				 *
				 * Allows developer to filter the "comment author says" text so it can say something different, or nothing at all.
				 *
				 * @since unknown
				 *
				 * @param string $text Comment author says text.
				 */
				$comment_author_says_text = apply_filters( 'comment_author_says_text', __( 'says', 'genesis' ) );

				printf( '<span itemprop="name">%s</span> <span class="says">%s</span>', $author, $comment_author_says_text );
				?>
		 	</p>

		</header>

		<div <?php echo genesis_attr( 'comment-content' ); ?>>
			<?php if ( ! $comment->comment_approved ) : ?>
				<?php
				/**
				 * Filter the "comment awaiting moderation" text.
				 *
				 * Allows developer to filter the "comment awaiting moderation" text so it can say something different, or nothing at all.
				 *
				 * @since unknown
				 *
				 * @param string $text Comment awaiting moderation text.
				 */
				$comment_awaiting_moderation_text = apply_filters( 'genesis_comment_awaiting_moderation', __( 'Your comment is awaiting moderation.', 'genesis' ) );
				?>
				<p class="alert"><?php echo $comment_awaiting_moderation_text; ?></p>
			<?php endif; ?>

			<?php comment_text(); ?>
		</div>

		<?php
		comment_reply_link( array_merge( $args, array(
			'depth'  => $depth,
			'before' => sprintf( '<div %s>', genesis_attr( 'comment-reply' ) ),
			'after'  => '</div>',
		) ) );
		?>

		<?php do_action( 'genesis_after_comment' ); ?>

	</article>
	<?php
	//* No ending </li> tag because of comment threading

}