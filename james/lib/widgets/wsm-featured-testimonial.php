<?php
/**
 * Modification of the Genesis Featured Page Widget
 * to add customizable text area option.
 *
 */


add_action( 'widgets_init', create_function( '', "register_widget('WSM_Featured_Testimonial');" ) );


class WSM_Featured_Testimonial extends WP_Widget {

	/**
	 * Constructor. Set the default widget options and create widget.
	 */
	function __construct() {
		$widget_ops = array( 'classname' => 'wsm-featured-testimonial', 'description' => __( 'Displays Custom Testimonial', 'james' ) );
		$control_ops = array( 'width' => 200, 'height' => 250, 'id_base' => 'wsm-featured-testimonial' );
		parent::__construct( 'wsm-featured-testimonial', __( 'Web Savvy - Featured Testimonial', 'james' ), $widget_ops, $control_ops );
	}

	/**
	 * Echo the widget content.
	 *
	 * @param array $args Display arguments including before_title, after_title, before_widget, and after_widget.
	 * @param array $instance The settings for the particular instance of the widget
	 */
	function widget($args, $instance) {
		extract($args);

		$instance = wp_parse_args( (array) $instance, array(
			'wsm-title' => '',
			'wsm-testimonial' => '',
			'wsm-testimonial_author' => '',
			'wsm-testimonial_company' => '',
			'wsm-testimonial_photo' => '',
		) );


		// WMPL
		/**
		 * Filter strings for WPML translation
     	 */
     	$instance['wsm-title'] = apply_filters( 'wpml_translate_single_string', $instance['wsm-title'], 'Widgets', 'Web Savvy - Featured Testimonial - Widget Title' );
     	$instance['wsm-testimonial'] = apply_filters( 'wpml_translate_single_string', $instance['wsm-testimonial'], 'Widgets', 'Web Savvy - Featured Testimonial - Testimonial' );
     	$instance['wsm-testimonial_author'] = apply_filters( 'wpml_translate_single_string', $instance['wsm-testimonial_author'], 'Widgets', 'Web Savvy - Featured Testimonial - Author' );
     	$instance['wsm-testimonial_company'] = apply_filters( 'wpml_translate_single_string', $instance['wsm-testimonial_company'], 'Widgets', 'Web Savvy - Featured Testimonial - Company' );
     	// WPML

		echo $before_widget;

		if ( ! empty( $instance['wsm-title'] ) ) {
			$heading = wp_kses_post($instance['wsm-title']);
			echo '<h3 class="widget-title widgettitle">'. $heading .'</h3>';
		}

		if(!empty($instance['wsm-testimonial'])) {
					$testimonial1 = wp_kses_post($instance['wsm-testimonial']);
			echo '<div class="testimonial1 featured-testimonial">';
					if(!empty($instance['wsm-testimonial_photo'])) { echo '<div class="testimonial-photo"><img class="featured-photo" src="' . $instance['wsm-testimonial_photo'] .'" alt="' . $instance['wsm-testimonial_author'] .'"/></div>'; }
					echo '<blockquote>' .$testimonial1 . '</blockquote>';
					echo '<div class="credit">';
					if(!empty($instance['wsm-testimonial_author'])) { echo '<span class="author">' . $instance['wsm-testimonial_author'] .'</span>'; }
					if(!empty($instance['wsm-testimonial_company'])) {	echo '<span class="company-name">' . $instance['wsm-testimonial_company'] .'</span>';}
					echo '</div>';
			echo '</div>';
		}


		echo $after_widget;
		wp_reset_query();
	}

	/** Update a particular instance.
	 *
	 * This function should check that $new_instance is set correctly.
	 * The newly calculated value of $instance should be returned.
	 * If "false" is returned, the instance won't be saved/updated.
	 *
	 * @param array $new_instance New settings for this instance as input by the user via form()
	 * @param array $old_instance Old settings for this instance
	 * @return array Settings to save or bool false to cancel saving
	 */
	function update($new_instance, $old_instance) {
		$new_instance['wsm-title'] = stripslashes( wp_filter_post_kses( addslashes($new_instance['wsm-title']) ) );
		$new_instance['wsm-testimonial'] = stripslashes( wp_filter_post_kses( addslashes($new_instance['wsm-testimonial']) ) );
		$new_instance['wsm-testimonial_author'] = strip_tags( $new_instance['wsm-testimonial_author'] );
		$new_instance['wsm-testimonial_company'] = strip_tags( $new_instance['wsm-testimonial_company'] );
		$new_instance['wsm-testimonial_photo'] = strip_tags( $new_instance['wsm-testimonial_photo'] );

		//WMPL
		/**
		 * register strings for translation
     	 */
	 	do_action( 'wpml_register_single_string', 'Widgets', 'Web Savvy - Featured Testimonial - Widget Title', $new_instance['wsm-title'] );
	 	do_action( 'wpml_register_single_string', 'Widgets', 'Web Savvy - Featured Testimonial - Testimonial', $new_instance['wsm-testimonial'] );
	 	do_action( 'wpml_register_single_string', 'Widgets', 'Web Savvy - Featured Testimonial - Author', $new_instance['wsm-testimonial_author'] );
	 	do_action( 'wpml_register_single_string', 'Widgets', 'Web Savvy - Featured Testimonial - Company', $new_instance['wsm-testimonial_company'] );
	 	//WMPL


		return $new_instance;
	}

	/** Echo the settings update form.
	 *
	 * @param array $instance Current settings
	 */
	function form($instance) {

		$instance = wp_parse_args( (array)$instance, array(
			'wsm-title' => '',
			'wsm-testimonial' => '',
			'wsm-testimonial_author' => '',
			'wsm-testimonial_company' => '',
			'wsm-testimonial_photo' => '',
		) );

		$title = esc_attr($instance['wsm-title']);
		$testimonial1 = esc_attr($instance['wsm-testimonial']);
	?>

		<p><label for="<?php echo $this->get_field_id( 'wsm-title' ); ?>"><?php _e( 'Title', 'james' ); ?></label>
		<input type="text" id="<?php echo $this->get_field_id( 'wsm-title' ); ?>" name="<?php echo $this->get_field_name( 'wsm-title' ); ?>" value="<?php echo $title; ?>" class="widefat" /></p>

		<p><label for="<?php echo $this->get_field_id( 'wsm-testimonial' ); ?>"><?php _e( 'Testimonial', 'james' ); ?></label><textarea class="widefat" rows="6" cols="20" id="<?php echo $this->get_field_id( 'wsm-testimonial' ); ?>" name="<?php echo $this->get_field_name( 'wsm-testimonial' ); ?>"><?php echo $testimonial1; ?></textarea></p>

		<p><label for="<?php echo $this->get_field_id( 'wsm-testimonial_author' ); ?>"><?php _e( 'Author', 'james' ); ?></label>
		<input type="text" id="<?php echo $this->get_field_id( 'wsm-testimonial_author' ); ?>" name="<?php echo $this->get_field_name( 'wsm-testimonial_author' ); ?>" value="<?php echo esc_attr( $instance['wsm-testimonial_author'] ); ?>" class="widefat" /></p>

		<p><label for="<?php echo $this->get_field_id( 'wsm-testimonial_company' ); ?>"><?php _e( 'Company', 'james' ); ?></label>
		<input type="text" id="<?php echo $this->get_field_id( 'wsm-testimonial_company' ); ?>" name="<?php echo $this->get_field_name( 'wsm-testimonial_company' ); ?>" value="<?php echo esc_attr( $instance['wsm-testimonial_company'] ); ?>" class="widefat" /></p>

		<p><label for="<?php echo $this->get_field_id('_photo'); ?>"><?php _e( 'Photo URL', 'james' ); ?></label>
		<input type="text" id="<?php echo $this->get_field_id( 'wsm-testimonial_photo' ); ?>" name="<?php echo $this->get_field_name( 'wsm-testimonial_photo' ); ?>" value="<?php echo esc_attr( $instance['wsm-testimonial_photo'] ); ?>" class="widefat" /></p>

	<?php
	}
}