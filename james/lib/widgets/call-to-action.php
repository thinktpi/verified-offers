<?php
/**
 * Modification of the james Featured Page Widget
 * to add customizable text area option.
 *
 */


add_action( 'widgets_init', create_function( '', "register_widget('WSM_CTA_Widget');" ) );


class WSM_CTA_Widget extends WP_Widget {

	/**
	 * Constructor. Set the default widget options and create widget.
	 */
	function __construct() {
		$widget_ops = array( 'classname' => 'wsm-cta-widget', 'description' => __( 'Displays icons and customizable headline and link', 'james' ) );
		$control_ops = array( 'width' => 200, 'height' => 250, 'id_base' => 'wsm-sidebar-cta-widget' );
		parent::__construct( 'wsm-sidebar-cta-widget', __( 'Web Savvy - CTA Widget', 'james' ), $widget_ops, $control_ops );
	}

	/**
	 * Echo the widget content.
	 *
	 * @param array $args Display arguments including before_title, after_title, before_widget, and after_widget.
	 * @param array $instance The settings for the particular instance of the widget
	 */
	function widget($args, $instance) {
		extract($args);

		$instance = wp_parse_args( (array) $instance, array(
			'title' => '',
			'wsm-title' => '',
			'wsm-content' => '',
			'wsm-morelink' => '',
			'wsm-moretarget' => '',
			'wsm-cta-icon' => '',
			'wsm-cta-icon-url' => '',
		) );


		// WMPL
		/**
		 * Filter strings for WPML translation
     	 */
     	$instance['title'] = apply_filters( 'wpml_translate_single_string', $instance['title'], 'Widgets', 'Web Savvy - CTA Widget - Title' );
     	$instance['wsm-title'] = apply_filters( 'wpml_translate_single_string', $instance['wsm-title'], 'Widgets', 'Web Savvy - CTA Widget - CTA Title' );
     	$instance['wsm-content'] = apply_filters( 'wpml_translate_single_string', $instance['wsm-content'], 'Widgets', 'Web Savvy - CTA Widget - Custom Text' );
     	$instance['wsm-morelink'] = apply_filters( 'wpml_translate_single_string', $instance['wsm-morelink'], 'Widgets', 'Web Savvy - CTA Widget - Link URL' );
     	$instance['wsm-cta-icon'] = apply_filters( 'wpml_translate_single_string', $instance['wsm-cta-icon'], 'Widgets', 'Web Savvy - CTA Widget - Icon' );
     	$instance['wsm-cta-icon-url'] = apply_filters( 'wpml_translate_single_string', $instance['wsm-cta-icon-url'], 'Widgets', 'Web Savvy - CTA Widget - Alt Icon' );
     	// WPML

		echo $before_widget;

			// Set up the CTA's

			if ( ! empty( $instance['title'] ) )
			echo $before_title . apply_filters( 'widget_title', $instance['title'], $instance, $this->id_base ) . $after_title;

			echo '<div class="cta-wrap">';

			// CTA 1

				echo '<div class="cta-box">';

					if (!empty( $instance['wsm-cta-icon'] ) ) {
						$icon1 = wp_kses_post($instance['wsm-cta-icon']);
						if (!empty( $instance['wsm-morelink'] ) ) {
							echo '<a href="'. esc_attr( $instance['wsm-morelink'] ) . '"  target="' . $instance['wsm-moretarget'] . '">';
							echo $icon1;
							echo '</a>';
						}
						else {
							echo $icon1;
						}
					}

					elseif ( !empty( $instance['wsm-cta-icon-url'] ) ) {
						if (!empty( $instance['wsm-morelink'] ) ) {
						echo '<a href="'. esc_attr( $instance['wsm-morelink'] ) . '"  target="' . $instance['wsm-moretarget'] . '"> <img height="97" width="126" class="cta-icon" src="'. esc_attr( $instance['wsm-cta-icon-url'] ) . '" alt="'. strip_tags( $instance['wsm-title'] ) . '"/></a>';
						}
						else {
						echo '<img class="cta-icon" height="97" width="126" src="'. esc_attr( $instance['wsm-cta-icon-url'] ) . '" alt="'. strip_tags( $instance['wsm-title'] ) . '"/>';
						}
					}

					if (!empty( $instance['wsm-title'] ) ) {
					if (!empty( $instance['wsm-morelink'] ) ) :
					$title1 = wp_kses_post($instance['wsm-title']);
						echo '<h3 class="cta-title">';
						echo'<a href="'. esc_attr( $instance['wsm-morelink'] ) . '" target="' . $instance['wsm-moretarget'] . '">';
						echo $title1 ;
						echo'</a>';
						echo '</h3>';
					else :
					$title1 = wp_kses_post($instance['wsm-title']);
							echo '<h3 class="cta-title">';
							echo $title1 ;
							echo '</h3>';
						endif;

					}

					if (!empty( $instance['wsm-content'] ) ) {
						echo '<div class="cta-box-content">';
						$text1 = wp_kses_post($instance['wsm-content']);
						echo $text1;
						echo '</div>';
					}

				echo '</div>';


			echo '</div>';


		echo $after_widget;

		wp_reset_query();

	}

	/** Update a particular instance.
	 *
	 * This function should check that $new_instance is set correctly.
	 * The newly calculated value of $instance should be returned.
	 * If "false" is returned, the instance won't be saved/updated.
	 *
	 * @param array $new_instance New settings for this instance as input by the user via form()
	 * @param array $old_instance Old settings for this instance
	 * @return array Settings to save or bool false to cancel saving
	 */
	function update($new_instance, $old_instance) {
		$new_instance['title']     = strip_tags( $new_instance['title'] );
		$new_instance['wsm-title'] = stripslashes( wp_filter_post_kses( addslashes($new_instance['wsm-title']) ) );
		$new_instance['wsm-content'] = stripslashes( wp_filter_post_kses( addslashes($new_instance['wsm-content']) ) ); // wp_filter_post_kses() expects slashed
		$new_instance['wsm-morelink'] = strip_tags( $new_instance['wsm-morelink'] );
		$new_instance['wsm-moretarget'] = strip_tags( $new_instance['wsm-moretarget'] );
		$new_instance['wsm-cta-icon'] = stripslashes( $new_instance['wsm-cta-icon'] );
		$new_instance['wsm-cta-icon-url'] = strip_tags( $new_instance['wsm-cta-icon-url'] );

		//WMPL
		/**
		 * register strings for translation
     	 */
	 	do_action( 'wpml_register_single_string', 'Widgets', 'Web Savvy - CTA Widget - Title', $new_instance['title'] );
	 	do_action( 'wpml_register_single_string', 'Widgets', 'Web Savvy - CTA Widget - CTA Title', $new_instance['wsm-title'] );
	 	do_action( 'wpml_register_single_string', 'Widgets', 'Web Savvy - CTA Widget - Custom Text', $new_instance['wsm-content'] );
	 	do_action( 'wpml_register_single_string', 'Widgets', 'Web Savvy - CTA Widget - Link URL', $new_instance['wsm-icon-morelink'] );
	 	do_action( 'wpml_register_single_string', 'Widgets', 'Web Savvy - CTA Widget - Icon', $new_instance['wsm-cta-icon'] );
	 	do_action( 'wpml_register_single_string', 'Widgets', 'Web Savvy - CTA Widget - Alt Icon', $new_instance['wsm-cta-icon-url'] );
	 	//WMPL

		return $new_instance;
	}

	/** Echo the settings update form.
	 *
	 * @param array $instance Current settings
	 */
	function form($instance) {

		$instance = wp_parse_args( (array)$instance, array(
			'title' => '',
			'wsm-title' => '',
			'wsm-content' => '',
			'wsm-morelink' => '',
			'wsm-moretarget' => '',
			'wsm-cta-icon' => '',
			'wsm-cta-icon-url' => '',
		) );

		$title1 = esc_attr($instance['wsm-title']);
		$text1 = esc_textarea($instance['wsm-content']);

?>

		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title', 'james' ); ?></label>
			<input type="text" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo esc_attr( $instance['title'] ); ?>" class="widefat" />
		</p>

		<p><label for="<?php echo $this->get_field_id( 'wsm-title' ); ?>"><?php _e( 'CTA Title', 'james' ); ?></label>
		<input type="text" id="<?php echo $this->get_field_id('wsm-title'); ?>" name="<?php echo $this->get_field_name( 'wsm-title' ); ?>" value="<?php echo $title1; ?>" class="widefat" /></p>

		<p><label for="<?php echo $this->get_field_id('wsm-content'); ?>"><?php _e( 'Custom Text', 'james' ); ?></label>
		<textarea class="widefat" rows="2" cols="10" id="<?php echo $this->get_field_id( 'wsm-content' ); ?>" name="<?php echo $this->get_field_name( 'wsm-content' ); ?>"><?php echo $text1; ?></textarea>

		<p><label for="<?php echo $this->get_field_id( 'wsm-morelink' ); ?>"><?php _e( 'Link URL', 'james' ); ?></label>
		<input type="text" id="<?php echo $this->get_field_id( 'wsm-morelink' ); ?>" name="<?php echo $this->get_field_name( 'wsm-morelink' ); ?>" value="<?php echo esc_attr( $instance['wsm-morelink'] ); ?>" class="widefat" /></p>

		<p><label for="<?php echo $this->get_field_id( 'wsm-moretarget' ); ?>"><?php _e( 'Link Target', 'james' ); ?> </label>
			<select id="<?php echo $this->get_field_id( 'wsm-moretarget' ); ?>" name="<?php echo $this->get_field_name( 'wsm-moretarget' ); ?>">
				<option value="_self" <?php selected( '_self', $instance['wsm-moretarget'] ); ?>><?php _e( '_self', 'james' ); ?></option>
				<option value="_blank" <?php selected( '_blank', $instance['wsm-moretarget'] ); ?>><?php _e( '_blank', 'james' ); ?></option>
			</select>
		</p>

		<p><label for="<?php echo $this->get_field_id( 'wsm-cta-icon' ); ?>"><?php _e( 'Icon', 'james' ); ?></label>
		<input type="text" id="<?php echo $this->get_field_id( 'wsm-cta-icon' ); ?>" name="<?php echo $this->get_field_name('wsm-cta-icon'); ?>" value="<?php echo esc_attr( $instance['wsm-cta-icon'] ); ?>" class="widefat" /></p>

		<p><label for="<?php echo $this->get_field_id( 'wsm-cta-icon-url' ); ?>"><?php _e( 'Alt Icon', 'james' ); ?></label>
		<input type="text" id="<?php echo $this->get_field_id( 'wsm-cta-icon-url' ); ?>" name="<?php echo $this->get_field_name( 'wsm-cta-icon-url' ); ?>" value="<?php echo esc_attr( $instance['wsm-cta-icon-url'] ); ?>" class="widefat" /></p>


	<?php
	}
}