<?php
/**
 * James Settings
 *
 * This file registers all of James specific Theme Settings, accessible from
 * Genesis --> James Settings.
 *
 * NOTE: Change out "James" in this file with name of theme and delete this note
 */

/**
 * Registers a new admin page, providing content and corresponding menu item
 * for the Child Theme Settings page.
 *
 * @since 1.0.0
 *
 * @package james
 * @subpackage James_Settings
 */
class James_Settings extends Genesis_Admin_Boxes {

	/**
	 * Create an admin menu item and settings page.
	 * @since 1.0.0
	 */
	function __construct() {

		// Specify a unique page ID.
		$page_id = 'james';

		// Set it as a child to genesis, and define the menu and page titles
		$menu_ops = array(
			'submenu' => array(
				'parent_slug' => 'genesis',
				'page_title'  => __( 'James Settings', 'james' ),
				'menu_title'  => __( 'James Settings', 'james' ),
				'capability' => 'manage_options',
			)
		);

		// Set up page options. These are optional, so only uncomment if you want to change the defaults
		$page_ops = array(
		//	'screen_icon'       => 'options-general',
		//	'save_button_text'  => 'Save Settings',
		//	'reset_button_text' => 'Reset Settings',
		//	'save_notice_text'  => 'Settings saved.',
		//	'reset_notice_text' => 'Settings reset.',
		);

		// Give it a unique settings field.
		// You'll access them from genesis_get_option( 'option_name', 'james-settings' );
		$settings_field = 'james-settings';

		// Set the default values
		$default_settings = array(
			'wsm_page_top_image' => '[url]/wp-content/themes/james/images/top-image.jpg',
			'wsm_copyright' => '[footer_copyright copyright="Copyright &copy; "] James Consulting Solutions | All Rights Reserved | WordPress Theme by Web Savvy Marketing',
		);

		// Create the Admin Page
		$this->create( $page_id, $menu_ops, $page_ops, $settings_field, $default_settings );

		// Initialize the Sanitization Filter
		add_action( 'genesis_settings_sanitizer_init', array( $this, 'sanitization_filters' ) );

	}

	/**
	 * Set up Sanitization Filters
	 * @since 1.0.0
	 *
	 * See /lib/classes/sanitization.php for all available filters.
	 */
	function sanitization_filters() {

		genesis_add_option_filter( 'no_html', $this->settings_field,
			array(
				'wsm_page_top_image',

			) );

		genesis_add_option_filter( 'safe_html', $this->settings_field,
			array(
				'wsm_copyright',
			) );

		genesis_add_option_filter( 'one_zero', $this->settings_field,
			array(
				'wsm_ignore_updates',
			) );
	}

	/**
	 * Set up Help Tab
	 * @since 1.0.0
	 *
	 * Genesis automatically looks for a help() function, and if provided uses it for the help tabs
	 * @link http://wpdevel.wordpress.com/2011/12/06/help-and-screen-api-changes-in-3-3/
	 */
	 function help() {
	 	$screen = get_current_screen();

		$screen->add_help_tab( array(
			'id'      => 'sample-help',
			'title'   => 'Sample Help',
			'content' => '<p>Help content goes here.</p>',
		) );
	 }

	/**
	 * Register metaboxes on Child Theme Settings page
	 * @since 1.0.0
	 */
	function metaboxes() {
		add_meta_box('wsm_main_theme_settings_metabox', __( 'James Theme Settings', 'james' ), array( $this, 'wsm_main_theme_settings_metabox' ), $this->pagehook, 'main', 'high' );
		add_meta_box('wsm_upate_notifications_metabox', __( 'Update Notifications', 'james' ), array( $this, 'wsm_upate_notifications_metabox' ), $this->pagehook, 'main', 'high');
	}

	/**
	 * James Theme Settings Metabox
	 * @since 1.0.0
	 */
	function wsm_main_theme_settings_metabox() {

	echo '<p><strong>' . __( 'Interior Page Default Image URL:', 'james' ) . '</strong> <em>' . __( 'size: 1252 x 275px', 'james' ) . '</em></p>';
	echo '<p><input class="large-text" type="text" name="' . $this->get_field_name( 'wsm_page_top_image' ) . '" id="' . $this->get_field_id( 'wsm_page_top_image' ) . '" value="' . esc_attr( $this->get_field_value( 'wsm_page_top_image' ) ) . '"/></p>';

	echo '<p><strong>' . __( 'Footer Copyright Info:', 'james' ) . '</strong></p>';
		echo '<p><input class="large-text" type="text" name="' . $this->get_field_name( 'wsm_copyright' ) . '" id="' . $this->get_field_id( 'wsm_copyright' ) . '" value="' . esc_attr( $this->get_field_value( 'wsm_copyright' ) ) . '" /></p>';

	}

	/**
	 * Update Notifications Metabox
	 * @since 1.0.0
	 */
	function wsm_upate_notifications_metabox() {

		echo '<p>' . __( 'Please check the box below if you wish to ignore/hide the theme update notification.<br/>Uncheck the box if you wish to be notified of theme updates.', 'james' ) . '</p>';

		echo '<input type="checkbox" name="' . $this->get_field_name( 'wsm_ignore_updates' ) . '" id="' .  $this->get_field_id( 'wsm_ignore_updates' ) . '" value="1" ';
		checked( 1, $this->get_field_value( 'wsm_ignore_updates' ) );
		echo '/> <label for="' . $this->get_field_id( 'wsm_ignore_updates' ) . '">' . __( 'Ignore Theme Updates?', 'james' ) . '</label>';

	}


}

/**
 * Add the Theme Settings Page
 * @since 1.0.0
 */
function james_add_settings() {
	global $_child_theme_settings;
	$_child_theme_settings = new James_Settings;
}
add_action( 'genesis_admin_menu', 'james_add_settings' );
