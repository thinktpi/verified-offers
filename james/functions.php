<?php



add_action( 'after_setup_theme', 'james_i18n' );

/**

 * Load the child theme textdomain for internationalization.

 *

 * Must be loaded before Genesis Framework /lib/init.php is included.

 * Translations can be filed in the /languages/ directory.

 *

 * @since 1.0.0

 */

function james_i18n() {

    load_child_theme_textdomain( 'james', get_stylesheet_directory() . '/languages' );

}



add_action( 'wp_enqueue_scripts', 'wsm_enqueue_assets' );

/**

 * Enqueue theme assets.

 */

function wsm_enqueue_assets() {

	wp_enqueue_script( 'theme-js', get_stylesheet_directory_uri() . '/js/main.js', array('jquery'), '1.0', true );

	wp_enqueue_style( 'james', get_stylesheet_uri() );

	wp_style_add_data( 'james', 'rtl', 'replace' );

}



//* Start the engine

require_once(TEMPLATEPATH.'/lib/init.php');

require_once( 'lib/init.php' );



//* Calls the theme's constants & files

james_init();



//* Editor Styles

add_editor_style( 'editor-style.css' );



//* Force Stupid IE to NOT use compatibility mode

add_filter( 'wp_headers', 'james_keep_ie_modern' );

function james_keep_ie_modern( $headers ) {

        if ( isset( $_SERVER['HTTP_USER_AGENT'] ) && ( strpos( $_SERVER['HTTP_USER_AGENT'], 'MSIE' ) !== false ) ) {

                $headers['X-UA-Compatible'] = 'IE=edge,chrome=1';

        }

        return $headers;

}



/** Create additional color style options */

add_theme_support( 'genesis-style-selector',

	array(

		'james-option1' => 'Color Option 1',

		'james-option2' => 'Color Option 2',

		'james-option3' => 'Color Option 3',

		'james-option4' => 'Color Option 4',

		'james-option5' => 'Color Option 5',

	)

);



//* Add new image sizes

add_image_size( 'blog-featured', 1152, 462, TRUE );

add_image_size( 'home-featured-image', 610, 338, TRUE );

add_image_size( 'sidebar-featured-image', 152, 152, TRUE );

add_image_size( 'testimonials', 268, 268, TRUE );





//* Relocate the post image (requires HTML5 theme support)

remove_action( 'genesis_entry_content', 'genesis_do_post_image', 8 );

add_action( 'genesis_entry_header', 'genesis_do_post_image', 5 );



//* Customize the Search Box

add_filter( 'genesis_search_button_text', 'custom_search_button_text' );

function custom_search_button_text( $text ) {

    return esc_attr( 'Go' );

}



//* Customize search form input box text

add_filter( 'genesis_search_text', 'sp_search_text' );

function sp_search_text( $text ) {

	return esc_attr( 'Search this site' );

}



//* Modify the author box display

add_filter( 'genesis_author_box', 'james_author_box' );

function james_author_box() {

	$authinfo = '';

	$authdesc = get_the_author_meta( 'description' );



	if( !empty( $authdesc ) ) {

		$authinfo .= sprintf( '<section %s>', genesis_attr( 'author-box' ) );

		$authinfo .= '<span class="avatar-box">'. get_avatar( get_the_author_id() , 300, '', get_the_author_meta( 'display_name' ) . '\'s avatar' ). '</span>';

		$authinfo .= '<h3 class="author-box-title">' . get_the_author_meta( 'display_name' ) . '</h3>';

		$authinfo .= '<div class="author-box-content" itemprop="description">';

		$authinfo .= '<p>' . get_the_author_meta( 'description' ) . '</p>';

		$authinfo .= '</div>';

		$authinfo .= '</section>';

	}

	if ( is_author() || is_single() ) {

		echo $authinfo;

	}

}





//* Customize the entry meta in the entry header (requires HTML5 theme support)

add_filter( 'genesis_post_info', 'sp_post_info_filter' );

function sp_post_info_filter( $post_info ) {

	$post_info = '[post_date before="" format="m.d.y"] [post_categories sep=", " before=""] [post_comments]';

	return $post_info;

}



//* Relocate the entry meta in the entry header (requires HTML5 theme support)

remove_action( 'genesis_entry_header', 'genesis_post_info', 12 );

add_action( 'genesis_entry_header', 'genesis_post_info', 8 );



//* Customize the post meta function

add_filter( 'genesis_post_meta', 'post_meta_filter' );

function post_meta_filter( $post_meta ) {

	if ( ! is_singular( 'post' ) )  return;

    $post_meta = '[post_tags sep=", " before="' . __( 'Tags: ', 'james' ) .  '"]';

    return $post_meta;

}



//* Add Read More button to blog page and archives

add_filter( 'excerpt_more', 'james_add_excerpt_more' );

add_filter( 'get_the_content_more_link', 'james_add_excerpt_more' );

add_filter( 'the_content_more_link', 'james_add_excerpt_more' );

function james_add_excerpt_more( $more ) {

    return '... <span class="more-link"><a href="' . get_permalink() . '" rel="nofollow">' . __( 'Read More', 'james' ) . '</a></span>';

}



/*

 * Add support for targeting individual browsers via CSS

 * See readme file located at /lib/js/css_browser_selector_readm.html

 * for a full explanation of available browser css selectors.

 */

add_action( 'get_header', 'james_load_scripts' );

function james_load_scripts() {

    wp_enqueue_script( 'browserselect', CHILD_URL . '/lib/js/css_browser_selector.js', array('jquery'), '0.4.0', TRUE );

}





//* Structural Wrap

add_theme_support( 'genesis-structural-wraps', array(

	'header',

	'subnav',

	'site-inner',

	'footer-widgets',

	'footer',

) );





//* Changes Default Navigation to Primary & Footer



add_theme_support ( 'genesis-menus' ,

	array (

		'primary' => 'Primary Navigation Menu' ,

		'secondary' => 'Secondary Navigation Menu' ,

	)

);

add_action( 'genesis_before_header', 'utility_bar' );



function utility_bar() {

 

	echo '<div class="utility-bar"><div class="wrap">';

 

	genesis_widget_area( 'utility-bar-left', array(

		'before' => '<div class="utility-bar-left">',

		'after' => '</div>',

	) );

 

	genesis_widget_area( 'utility-bar-right', array(

		'before' => '<div class="utility-bar-right">',

		'after' => '</div>',

	) );

 

	echo '</div></div>';

 

}

//* Reposition the primary navigation menu

remove_action( 'genesis_after_header', 'genesis_do_nav' );

add_action( 'genesis_header_right', 'genesis_do_nav' );



//* Reposition the secondary navigation menu

remove_action( 'genesis_after_header', 'genesis_do_subnav' );

add_action( 'genesis_before_header', 'genesis_do_subnav' );



//* Unregister Layouts

genesis_unregister_layout( 'content-sidebar-sidebar' );

genesis_unregister_layout( 'sidebar-sidebar-content' );

genesis_unregister_layout( 'sidebar-content-sidebar' );



//* Add support for 4 footer widgets

add_theme_support( 'genesis-footer-widgets', 4);

/** Register Utility Bar Widget Areas. */



genesis_register_sidebar( array(

'id' => 'utility-bar-left',

'name' => __( 'Utility Bar Left', 'james' ),

'description' => __( 'This is the left utility bar above the header.', 'theme-prefix' ),

) );



genesis_register_sidebar( array(

'id' => 'utility-bar-right',

'name' => __( 'Utility Bar Right', 'theme-prefix' ),

'description' => __( 'This is the right utility bar above the header.', 'theme-prefix' ),

) );

//* Setup Sidebars

unregister_sidebar( 'sidebar-alt' );

unregister_sidebar( 'header-right' );



genesis_register_sidebar( array(

	'id'			=> 'home-f1',

	'name'			=> __( 'Home Feature 1', 'james' ),

	'description'	=> __( 'This is the home page features section.', 'james' ),

) );

genesis_register_sidebar( array(

	'id'			=> 'home-f2',

	'name'			=> __( 'Home Feature 2', 'james' ),

	'description'	=> __( 'This is the home page features section.', 'james' ),

) );

genesis_register_sidebar( array(

	'id'			=> 'home-f3',

	'name'			=> __( 'Home Feature 3', 'james' ),

	'description'	=> __( 'This is the home page features section.', 'james' ),

) );

genesis_register_sidebar( array(

	'id'			=> 'home-f4',

	'name'			=> __( 'Home Feature 4', 'james' ),

	'description'	=> __( 'This is the home page features section.', 'james' ),

) );

genesis_register_sidebar( array(

	'id'			=> 'home-f5',

	'name'			=> __( 'Home Feature 5', 'james' ),

	'description'	=> __( 'This is the home page features section.', 'james' ),

) );

genesis_register_sidebar( array(

	'id'			=> 'home-f6',

	'name'			=> __( 'Home Feature 6', 'james' ),

	'description'	=> __( 'This is the home page features section.', 'james' ),

) );

genesis_register_sidebar( array(

	'id'			=> 'blog-sidebar',

	'name'			=> __( 'Blog Sidebar', 'james' ),

	'description'	=> __( 'This is the Blog Page Sidebar.', 'james' ),

) );

genesis_register_sidebar( array(

	'id'			=> 'page-sidebar',

	'name'			=> __( 'Page Sidebar', 'james' ),

	'description'	=> __( 'This is the Page Sidebar.', 'james' ),

) );

genesis_register_sidebar( array(

	'id'			=> 'contact-sidebar',

	'name'			=> __( 'Contact Us Sidebar', 'james' ),

	'description'	=> __( 'This is the Contact Page Sidebar.', 'james' ),

) );

genesis_register_sidebar( array(

	'id'			=> 'interior-footer-cta',

	'name'			=> __( 'Footer Call to Action', 'james' ),

	'description'	=> __( 'This is the interior pages footer call to action section.', 'james' ),

) );





//* Remove edit link from TablePress tables for logged in users

add_filter( 'tablepress_edit_link_below_table', '__return_false' );





//* Display Category Title

add_filter( 'genesis_term_meta_headline', 'be_default_category_title', 10, 2 );

function be_default_category_title( $headline, $term ) {

	if( ( is_category() || is_tag() || is_tax() ) && empty( $headline ) ) {

		$headline = $term->name;

	}

	return $headline;

}



//* Reposition the breadcrumbs

remove_action( 'genesis_before_loop', 'genesis_do_breadcrumbs' );

add_action( 'genesis_before_content_sidebar_wrap', 'genesis_do_breadcrumbs', 5 );



//* Modify breadcrumb arguments.

add_filter( 'genesis_breadcrumb_args', 'sp_breadcrumb_args' );

function sp_breadcrumb_args( $args ) {

	$args['home'] = 'Home';

	$args['sep'] = ' <span>|</span> ';

	$args['list_sep'] = ', '; // Genesis 1.5 and later

	$args['prefix'] = '<div class="breadcrumb">';

	$args['suffix'] = '</div>';

	$args['heirarchial_attachments'] = true; // Genesis 1.5 and later

	$args['heirarchial_categories'] = true; // Genesis 1.5 and later

	$args['display'] = true;

	$args['labels']['prefix'] = '';

	$args['labels']['author'] = '';

	$args['labels']['category'] = ''; // Genesis 1.6 and later

	$args['labels']['tag'] = '';

	$args['labels']['date'] = '';

	$args['labels']['search'] = 'Search for ';

	$args['labels']['tax'] = '';

	$args['labels']['post_type'] = '';

	$args['labels']['404'] = 'Not found: '; // Genesis 1.5 and later

return $args;

}



add_shortcode( 'james-button', 'james_button_shortcode' );

function james_button_shortcode( $atts, $content = null ) {

	$a = shortcode_atts( array(

		'class' => 'red',

		'link' => '#',

		'target' => '_self',

	), $atts );



	return '<div class="cta-button ' . esc_attr( $a['class'] ) . '"><a href="' . esc_attr( $a['link'] ) . '" target="' . esc_attr( $a['target'] ) . '">' . $content . '</a></div>';

}





//* Remove Genesis Page Templates

add_filter( 'theme_page_templates', 'james_remove_genesis_page_templates' );

function james_remove_genesis_page_templates( $page_templates ) {

	unset( $page_templates['page_blog.php'] );

	return $page_templates;

}





//* Force Full Width Blog Layout

add_action( 'get_header', 'james_blog_layout' );

function james_blog_layout() {

	if ( is_home() || is_archive() || is_tag() || is_category() ) {

		add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_full_width_content' );

	}

}



//* Relocate after entry widgetarea

remove_action( 'genesis_after_entry', 'genesis_after_entry_widget_area' );

add_action( 'genesis_after_entry', 'genesis_after_entry_widget_area', 5 );





//* Add Interior Pages Footer Call to Action

add_action( 'genesis_before_footer', 'james_interior_footer_cta', 20 );

function james_interior_footer_cta() {

	if ( is_front_page() ) { return; }

	genesis_widget_area( 'interior-footer-cta', array( 'before' => '<div class="interior-footer-cta widget-area"><div class="wrap">', 'after' => '</div></div>') );

}



// Relocate Footer Widgets

remove_action( 'genesis_before_footer', 'genesis_footer_widget_areas' );

add_action( 'genesis_before_footer', 'genesis_footer_widget_areas',25 );





//* Search Page Layout



add_action( 'wp_head', 'james_search_layout');

function james_search_layout() {

	if ( is_search() ) {

		//* Force full-width-content layout setting

		add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_full_width_content' );

	}

}