<?php

/**

Template Name: Home

**/

do_action( 'genesis_home' );

//* Force full-width-content layout setting
add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_full_width_content' );

// Add Top Banner
add_action( 'genesis_after_header' , 'james_home_top_banner' , 10 );
function james_home_top_banner() {

	global $post;

	$home_banner_image = get_post_meta( $post->ID, '_james_home_banner_image', true );
	$home_banner_image_alt = get_post_meta( $post->ID, '_james_home_banner_image_alt', true );
	$home_banner_title = get_post_meta( $post->ID, '_james_home_banner_title', true );
	$home_banner_text = get_post_meta( $post->ID, '_james_home_banner_text', true );

	if ( !empty( $home_banner_image ) ) {
		echo '<div class="top-image home-image"><div class="wrap">';

		echo '<img src="' . $home_banner_image . '" alt="' . $home_banner_image_alt . '" class="banner-image" />';

			echo '<div class="banner-text">';
			if ( !empty( $home_banner_title ) ) {
				echo '<h1 class="banner-title">' . $home_banner_title . '</h1>';
			}
			if ( !empty( $home_banner_text ) ) {
				echo '<p>' . $home_banner_text . '</p>';
			}
			echo '</div>';

		echo '</div></div>';
	}

}


// Remove the standard loop
remove_action( 'genesis_loop', 'genesis_do_loop' );

// Execute custom child loop
add_action( 'genesis_loop', 'james_home_loop_helper' );
function james_home_loop_helper() {

	genesis_widget_area( 'home-f1', array( 'before' => '<div class="home-f1 widget-area"><div class="wrap">', 'after' => '</div></div>') );
	genesis_widget_area( 'home-f2', array( 'before' => '<div class="home-f2 widget-area">', 'after' => '</div>') );
	genesis_widget_area( 'home-f3', array( 'before' => '<div class="home-f3 widget-area">', 'after' => '</div>') );
	genesis_widget_area( 'home-f4', array( 'before' => '<div class="home-f4 widget-area"><div class="wrap">', 'after' => '</div></div>') );
	genesis_widget_area( 'home-f5', array( 'before' => '<div class="home-f5 widget-area">', 'after' => '</div>') );
	genesis_widget_area( 'home-f6', array( 'before' => '<div class="home-f6 widget-area">', 'after' => '</div>') );
	genesis_widget_area( 'home-f7', array( 'before' => '<div class="home-f7 widget-area">', 'after' => '</div>') );

}

genesis();